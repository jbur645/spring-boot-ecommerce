Springboot Ecommerce Backend Version 0.0.1.

This is a full stack application deployed on AWS ec2. The ec2 instance has NGINX and Tomcat configured to run the frontend and the backend seprately.
Access to the AWS console is needed for maintience. An IAM identity is required provisioned by an adminstrator. Once access is granted the ec2 instance and the RDS can be viewd and monitored for issues. An SSL connection is reuierd to access the ec2. Once accessed configurations of nginx and tomcat can be implmented if necessary.

The unit testing is present in the spring project. Unit testing was implement to check the functionality of getters in the Entity class.


